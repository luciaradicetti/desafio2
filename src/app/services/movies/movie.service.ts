import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { Observable } from 'rxjs';
import { TvPopular } from 'src/app/components/interfaces/TvPopular';
import { MoviePopular } from 'src/app/components/interfaces/MoviePopular';
import { Trending } from 'src/app/components/interfaces/Trending';
import { AngularFirestore} from '@angular/fire/compat/firestore'
import { MovieSerieBase, MovieSerieUser } from 'src/app/components/interfaces/MovieSerie';
import { Result } from 'src/app/components/interfaces/result';

@Injectable({
  providedIn: 'root'
})
export class MoviesService {

  private appId: string ='e1ee3b89560d631235f6583338e051c4';
  private baseUrl: string = 'https://api.themoviedb.org/3/';
  
  
  constructor(
    private _http : HttpClient, private firestore: AngularFirestore
  ) { }

   getMovie(): Observable<MoviePopular> {
    let params = new HttpParams().set('api_key', this.appId);
    
    return this._http.get<MoviePopular>(this.baseUrl + 'movie/popular', {params: params});
   }
   getTrending(): Observable<Trending> {
    let params = new HttpParams().set('api_key', this.appId);
    
    return this._http.get<Trending>(this.baseUrl + 'trending/all/week', {params: params});
   }
   getSeries(): Observable<TvPopular> {
    let params = new HttpParams().set('api_key', this.appId);
    
    return this._http.get<TvPopular>(this.baseUrl + 'tv/popular', {params: params});
   }

   addUser(userId?: MovieSerieUser): Promise <any> {
    return this.firestore.collection('usuarios').add(userId);
   }

   addMovieSerie (userId: MovieSerieUser, item: MovieSerieBase, type:string ): Promise <any>{
     if (type == 'tv'){
      return this.firestore.collection('usuarios').doc(`${userId}`).collection('tv').add(item);
      
   } else {
    return this.firestore.collection('usuarios').doc(`${userId}`).collection('movie').add(item);
   }
  }
  getList(userId: MovieSerieUser, type:string): Observable<any>{
    if(type == 'tv'){
      return this.firestore.collection('usuarios').doc(`${userId}`).collection('tv').snapshotChanges();
    }else{
      return this.firestore.collection('usuarios').doc(`${userId}`).collection('movie').snapshotChanges();
    }
  }
  deleteItem(userId:string, id: string, type:string): Promise <any>{
    if(type == 'tv'){
      console.log("Este es el id", id)
      return this.firestore.collection(`usuarios/${userId}/tv`).doc(id).delete();
    }else{
      console.log("Este es el id", id)
      return this.firestore.collection(`usuarios/${userId}/movie`).doc(id).delete();
    }
  }
  
}
