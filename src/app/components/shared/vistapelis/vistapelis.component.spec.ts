import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VistapelisComponent } from './vistapelis.component';

describe('VistapelisComponent', () => {
  let component: VistapelisComponent;
  let fixture: ComponentFixture<VistapelisComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VistapelisComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VistapelisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
