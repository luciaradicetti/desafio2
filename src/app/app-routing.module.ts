import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AgregarComponent } from './components/routes/agregar/agregar.component';
import { IngresoComponent } from './components/routes/ingreso/ingreso.component';
import { InicioComponent } from './components/routes/inicio/inicio.component';
import { ListaComponent } from './components/routes/lista/lista.component';
import { ListadoSeriesComponent } from './components/routes/listado-series/listado-series.component';
import { ListadoComponent } from './components/routes/listado/listado.component';
import { PelisComponent } from './components/routes/pelis/pelis.component';
import { RegistroComponent } from './components/routes/registro/registro.component';
import { SeriesComponent } from './components/routes/series/series.component';
import { VistaComponent } from './components/shared/vista/vista.component';
import { VistapelisComponent } from './components/shared/vistapelis/vistapelis.component';

const routes: Routes = [
  {
    path: 'inicio',
    component: InicioComponent
  },
  {
    path: 'pelis',
    component: PelisComponent
  },
  {
    path: 'series',
    component: SeriesComponent
  },
  {
    path: 'ingreso',
    component: IngresoComponent
  },
  {
    path:'vista/:id',
    component: VistaComponent
  },
  {
    path:'vistapelis/:id',
    component: VistapelisComponent
  },
  {
    path:'registro',
    component: RegistroComponent
  },
  {
    path:'Bienvenido',
    component: ListaComponent
  },
  {
    path:'Peliculas',
    component: ListadoComponent
  },
  {
    path:'Series',
    component: ListadoSeriesComponent
  },
  {
    path:'agregar',
    component: AgregarComponent
  },
  {
    path: '**',
    redirectTo: 'inicio'
  },
  {
    path: ' ',
    redirectTo: 'inicio'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
