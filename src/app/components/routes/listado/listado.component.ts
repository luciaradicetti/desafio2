import { Component, OnInit } from '@angular/core';
import { MoviesService } from 'src/app/services/movies/movie.service';
import { MovieSerieBase } from '../../interfaces/MovieSerie';

@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html',
  styleUrls: ['./listado.component.css']
})
export class ListadoComponent implements OnInit {

  movies!: MovieSerieBase[] ;
  elemento!:string;
  filtro:MovieSerieBase[]=[];
  filtro_busqueda:MovieSerieBase[]=this.movies;
  movie:string="movie";
  user!:any;

  constructor(private _movieService:MoviesService) { }

  ngOnInit(): void {
    this.getMovie()
  }

  getMovie(){
    this.user = JSON.parse(localStorage.getItem('Usuario')  || '');
    this._movieService.getList(this.user.uid,"movie").subscribe(
      response => {
        this.movies =[];
        console.log("Esto es response",response)
        response.forEach((elemento:any)=>{
          console.log(elemento.payload.doc.id)
          console.log((elemento.payload.doc.data().type))
            this.movies.push({
              idglobal: elemento.payload.doc.id,
              ...elemento.payload.doc.data()
            })
        })
        console.log("Esta es la peticion de movies", this.movies)
        this.filtro_busqueda=this.movies;
      },
      error =>{
        console.log("Error peticion", error)
      }
    )
  }
  public contador(){
    return this.movies.length
  }
  
  public busqueda(valor:string){
    this.elemento = valor.toUpperCase();
    this.filtro=[];
    for(let card of this.movies){
      if(card.title?.toUpperCase().includes(this.elemento)){
          this.filtro.push(card);
       
      }
      if(valor != ''){
        this.filtro_busqueda=this.filtro;
        
      }else{
        this.filtro_busqueda=this.movies;
    
      }
    }
  }
}
