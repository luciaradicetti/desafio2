import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/Auth/auth.service';
import { MoviesService } from 'src/app/services/movies/movie.service';
import { MovieSerieUser } from '../../interfaces/MovieSerie';

@Component({
  selector: 'app-ingreso',
  templateUrl: './ingreso.component.html',
  styleUrls: ['./ingreso.component.css'],
  providers : [AuthService]
})
export class IngresoComponent implements OnInit {
  miFormulario: FormGroup = this.formulario.group({
    email: [, [Validators.required, Validators.minLength(6)]],
    password: [, [Validators.required, Validators.minLength(6)]],
  });
  bandera:boolean=false;
  datos!:any;
  user!: MovieSerieUser;
  
  constructor( private formulario: FormBuilder, private _authService:AuthService, private firestore:MoviesService, private router : Router) {}

  ngOnInit(): void {
 
  }

  campoEsValido(campo: string) {
    return (
      this.miFormulario.controls[campo].errors &&
      this.miFormulario.controls[campo].touched
    );
  }
  registro(){
    this.router.navigate([`registro`]);
  }

  guardar() {
    if (this.miFormulario.invalid) {
      this.miFormulario.markAllAsTouched();
      return;
    }
    this._authService.login(this.miFormulario.value.email,this.miFormulario.value.password).then(res =>{
      if(!res)
        return
    console.log("Se registro: ", res);
    localStorage.setItem('Usuario', JSON.stringify(res.user));
    this.bandera=true;
    this.miFormulario.reset();
    this.router.navigate([`Bienvenido`]);
  })
  }

  IngresarConGoogle(){
   this._authService.loginWithGoogle().then(res =>{
    if(!res)
      return
    console.log("Se registro con Google ", res);
    localStorage.setItem('Usuario',JSON.stringify(res.user));
    this.router.navigate([`Bienvenido`]);
  })
  }

  getDatos(){
   this._authService.getUserLogged().subscribe({
    next: (data : any) =>{
      this.datos=data;
      localStorage.setItem('Datos_Email', JSON.stringify(this.datos.email))
    },
    error:(err) =>{
      console.log(err)
    }
  })
  }

  cerrar(){
    this._authService.logout;
  }

  
}
