import { Component, OnInit } from '@angular/core';
import { MoviesService } from 'src/app/services/movies/movie.service';
import { MovieSerieBase } from '../../interfaces/MovieSerie';
import { Result } from '../../interfaces/result';
import {Trending } from '../../interfaces/Trending';

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.component.html',
  styleUrls: ['./agregar.component.css']
})
export class AgregarComponent implements OnInit {
  user!:any;
  aux:boolean=false;
  auxenvio:boolean=true;
  auxElim:boolean=true;
  i!:number;
  movie_tv!: Result[];   /*VER*/
  elemento!:string;
  filtro:Result[]=[];
  filtro_busqueda:Result[]=this.movie_tv;
  filter:string='Todos';
  arreMovie!: MovieSerieBase[];
  arreSerie!: MovieSerieBase[];
  constructor(private _movieService:MoviesService) { }
  ngOnInit(): void {
   this.get();
    setTimeout(() => {
      this.getTrending()
     },2000);
     
  }
  estado(){
    for(let card of this.filtro_busqueda){
      console.log("id",card.id)
      if(card.media_type == 'movie'){
        var buscar = this.buscar(card.id)
        console.log("resultado", buscar)
        if(buscar){
          card.estado=false;
          card.elim=true;
        }else{
          card.estado=true;
          card.elim=false;
              }
      }
      if(card.media_type=='tv'){
        var buscarS = this.buscarS(card.id)
        if(buscarS){
          card.estado=false;
          card.elim=true;
        }else{
          card.estado=true;
          card.elim=false;
        }
      }
    }
  }
buscar(id:string){
  this.aux=false;
  this.i=0;
    while(this.aux == false){
      if((this.i < this.arreMovie.length) == true){
        console.log("el i", this.i)
        console.log("tamaño", this.arreMovie.length)
        if((this.arreMovie[this.i].id!=id)){
          this.i++;
        
        }else{
          this.aux=true;
        }
      }else{
        this.aux=true;
      }
  }
  
  if(this.i < this.arreMovie.length){
    return true
  }else{
    return false
  }
  
}
buscarS(id:string){
  this.aux=false;
  this.i=0;
    while(this.aux == false){
      if((this.i < this.arreSerie.length) == true){
        if((this.arreSerie[this.i].id!=id)){
          this.i++;
        }else{
          this.aux=true;
          return true;
        }
      }else{
        this.aux=true;
      }
    }
    if(this.i < this.arreSerie.length){
      return true
    }else{
      return false
    }
  
}
getTrending(){
  this._movieService.getTrending().subscribe({
    next: (data : Trending) =>{
      this.movie_tv=data.results;
      this.filtro_busqueda=this.movie_tv;
      this.estado();
    },
    error: (err) =>{
      console.log(err);
    }
  })
}

get(){
  this.user = JSON.parse(localStorage.getItem('Usuario')  || '');
  this._movieService.getList(this.user.uid,"movie").subscribe(
    response => {
      this.arreMovie =[];
      console.log("Esto es response",response)
      response.forEach((elemento:any)=>{
        console.log(elemento.payload.doc.data())
          this.arreMovie.push({
            idglobal: elemento.payload.doc.id,
            ...elemento.payload.doc.data()
          })
      })
      console.log("Esta es la peticion de movies", this.arreMovie)
    },
    error =>{
      console.log("Error peticion", error)
    }
  )
  this._movieService.getList(this.user.uid,"tv").subscribe(
    response => {
      this.arreSerie =[];
      console.log("Esto es response",response)
      response.forEach((elemento:any)=>{
        console.log(elemento.payload.doc.data())
          this.arreSerie.push({
            idglobal: elemento.payload.doc.id,
            ...elemento.payload.doc.data()
          })
      })
      console.log("Esta es la peticion de movies", this.arreSerie)
    },
    error =>{
      console.log("Error peticion", error)
    }
  )
}

public contador(){
  return this.movie_tv.length
 
}

public busqueda(valor:string){
  this.elemento = valor.toUpperCase();
  this.filtro=[];
  for(let card of this.movie_tv){
    if(card.media_type == 'movie'){
      if(card.original_title.toUpperCase().includes(this.elemento)){
        this.filtro.push(card);
    }}
    if(card.media_type == 'tv'){
      if(card.name.toUpperCase().includes(this.elemento)){
        this.filtro.push(card);
      }
    }
    if(valor != ''){
      this.filtro_busqueda=this.filtro;
    }else{
      this.filtro_busqueda=this.movie_tv;

    }
  }
}

}