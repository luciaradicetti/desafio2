// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyC4kqIQpywX0USIFOV2ScaHGMTjXaitvg0",
    authDomain: "desafio-7-72cd2.firebaseapp.com",
    projectId: "desafio-7-72cd2",
    storageBucket: "desafio-7-72cd2.appspot.com",
    messagingSenderId: "201053322545",
    appId: "1:201053322545:web:d3d750a71b601d36e2c410",
    measurementId: "G-SQW14KT5WQ"
  }
};
const app = initializeApp(environment.firebaseConfig);
const analytics = getAnalytics(app);
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
