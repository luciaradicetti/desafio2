import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MoviesService } from 'src/app/services/movies/movie.service';
import { MovieSerieBase } from '../../interfaces/MovieSerie';

@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.css']
})
export class ListaComponent implements OnInit {
  user!:any;
  movies!: MovieSerieBase[] ;
  serie!: MovieSerieBase[] ;

  constructor(private router : Router,private _movieService:MoviesService) { }

  ngOnInit(): void {
    this.getMovie();
    this.getSerie();
  }
  getMovie(){
    this.user = JSON.parse(localStorage.getItem('Usuario')  || '');
    this._movieService.getList(this.user.uid,"movie").subscribe(
      response => {
        this.movies =[];
        console.log("Esto es response",response)
        response.forEach((elemento:any)=>{
          console.log(elemento.payload.doc.id)
          console.log((elemento.payload.doc.data().type))
            this.movies.push({
              idglobal: elemento.payload.doc.id,
              ...elemento.payload.doc.data()
            })
        })
      },
      error =>{
        console.log("Error peticion", error)
      }
    )
  }
  getSerie(){
    this.user = JSON.parse(localStorage.getItem('Usuario')  || '');
    this._movieService.getList(this.user.uid,"tv").subscribe(
      response => {
        this.serie =[];
        console.log("Esto es response",response)
        response.forEach((elemento:any)=>{
          console.log(elemento.payload.doc.data())
            this.serie.push({
              idglobal: elemento.payload.doc.id,
              ...elemento.payload.doc.data()
            })
        })
      },
      error =>{
        console.log("Error peticion", error)
      }
    )
  }
  public contador(){
      return this.movies.length
  }
  public contador1(){
    return this.serie.length
}

  peliculas(){
    this.router.navigate([`Peliculas`])
  }
  tv(){
    this.router.navigate([`Series`])
  }
}
