import { Component, OnInit } from '@angular/core';
import { Result, TvPopular } from '../../interfaces/TvPopular';
import { MoviesService } from 'src/app/services/movies/movie.service';

@Component({
  selector: 'app-series',
  templateUrl: './series.component.html',
  styleUrls: ['./series.component.css'],
  providers : [MoviesService]
})
export class SeriesComponent implements OnInit {
  tvs!: Result[];
  tv:string="tv";
  elemento!:string;
  filtro:Result[]=[];
  filtro_busqueda:Result[]=this.tvs;
  filter:string='Todos';
  constructor(private _movieService:MoviesService) { }

  ngOnInit(): void {
    this.getSeries()
  }
  getSeries(){
    this._movieService.getSeries().subscribe({
      next: (data : TvPopular) =>{
        console.log(data);
        this.tvs=data.results;
        this.filtro_busqueda=this.tvs;
      },
      error: (err)=>{
        console.log(err);
      }
    })
  }
  public contador(){
    return this.tvs.length
  }
  
  public busqueda(valor:string){
    this.elemento = valor.toUpperCase();
    this.filtro=[];
    for(let card of this.tvs){
      if(card.name.toUpperCase().includes(this.elemento)){
          this.filtro.push(card);
      }
      if(valor != ''){
        this.filtro_busqueda=this.filtro;
      }else{
        this.filtro_busqueda=this.tvs;
  
      }
    }
  }
}
