export interface Result {
    overview:          string;
    release_date?:     Date;
    id:                string;
    adult?:            boolean;
    backdrop_path:     string;
    genre_ids:         number[];
    vote_count:        number;
    original_language: OriginalLanguage;
    original_title:    string;
    poster_path:       string;
    title:             string;
    video?:            boolean;
    vote_average:      number;
    popularity:        number;
    media_type:        MediaType;
    original_name?:    string;
    origin_country?:   string[];
    first_air_date?:   Date;
    name:              string;
    estado?:            boolean;
    elim?:              boolean;
    
}

export enum MediaType {
    Movie = "movie",
    Tv = "tv",
}

export enum OriginalLanguage {
    En = "en",
    Ja = "ja",
}