import { Component, OnInit } from '@angular/core';
import { MoviePopular } from '../../interfaces/MoviePopular';
import { MoviesService } from 'src/app/services/movies/movie.service';
import { Result } from '../../interfaces/MoviePopular';
@Component({
  selector: 'app-pelis',
  templateUrl: './pelis.component.html',
  styleUrls: ['./pelis.component.css'],
  providers : [MoviesService]
})
export class PelisComponent implements OnInit {
  movies!: Result[] ;
  elemento!:string;
  filtro:Result[]=[];
  filtro_busqueda:Result[]=this.movies;
  movie:string="movie";

  constructor(private _movieService:MoviesService) { }

  ngOnInit(): void {
    this.getMovie()
  }

  getMovie(){
    this._movieService.getMovie().subscribe({
      next: (data : MoviePopular) =>{
        this.movies=data.results;
        this.filtro_busqueda=this.movies;
      },
      error:(err) =>{
        console.log(err)
      }
    })
  }
  public contador(){
    return this.movies.length
  }
  
  public busqueda(valor:string){
    this.elemento = valor.toUpperCase();
    this.filtro=[];
    for(let card of this.movies){
      if(card.original_title.toUpperCase().includes(this.elemento)){
          this.filtro.push(card);
      }
      if(valor != ''){
        this.filtro_busqueda=this.filtro;
      }else{
        this.filtro_busqueda=this.movies;
  
      }
    }
  }
}
