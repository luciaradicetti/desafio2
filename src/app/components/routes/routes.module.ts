import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InicioComponent } from './inicio/inicio.component';
import { PelisComponent } from './pelis/pelis.component';
import { SeriesComponent } from './series/series.component';
import { IngresoComponent } from './ingreso/ingreso.component';
import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { RouterModule } from '@angular/router';
import { RegistroComponent } from './registro/registro.component';
import { ListaComponent } from './lista/lista.component';
import { AgregarComponent } from './agregar/agregar.component';
import { ListadoComponent } from './listado/listado.component';
import { ListadoSeriesComponent } from './listado-series/listado-series.component';



@NgModule({
  declarations: [
    InicioComponent,
    PelisComponent,
    SeriesComponent,
    IngresoComponent,
    RegistroComponent,
    ListaComponent,
    AgregarComponent,
    ListadoComponent,
    ListadoSeriesComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    AppRoutingModule,
    RouterModule,
    ReactiveFormsModule
  ],
  exports:[
    InicioComponent,
    PelisComponent,
    SeriesComponent,
    IngresoComponent, 
    RegistroComponent,
    ListaComponent,
    AgregarComponent,
    ListaComponent,
    ListadoSeriesComponent
  ]
})
export class RoutesModule { }
