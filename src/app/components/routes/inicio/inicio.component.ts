import { Component, OnInit } from '@angular/core';
import { MoviesService } from 'src/app/services/movies/movie.service';
import { Result } from '../../interfaces/result';
import {Trending } from '../../interfaces/Trending';



@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css'],
  providers : [MoviesService]
})

export class InicioComponent implements OnInit {
  
  movie_tv!: Result[];
  elemento!:string;
  filtro:Result[]=[];
  filtro_busqueda:Result[]=this.movie_tv;
  filter:string='Todos';
  constructor(private _movieService:MoviesService) { }
  ngOnInit(): void {
    this.getTrending();

  }

getTrending(){
  this._movieService.getTrending().subscribe({
    next: (data : Trending) =>{
      this.movie_tv=data.results;
      this.filtro_busqueda=this.movie_tv;
    },
    error: (err) =>{
      console.log(err);
    }
  })
}


public contador(){
  if(this.filter == 'Todos'){
    return this.movie_tv.length
  }
  let total:number = 0
  this.movie_tv.map(movie => {
    if(movie.media_type == this.filter){
      total++
    }
  })
  return total
}

public busqueda(valor:string){
  this.elemento = valor.toUpperCase();
  this.filtro=[];
  for(let card of this.movie_tv){
    if(card.media_type == 'movie'){
      if(card.original_title.toUpperCase().includes(this.elemento)){
        this.filtro.push(card);
    }}
    if(card.media_type == 'tv'){
      if(card.name.toUpperCase().includes(this.elemento)){
        this.filtro.push(card);
      }
    }
    if(valor != ''){
      this.filtro_busqueda=this.filtro;
    }else{
      this.filtro_busqueda=this.movie_tv;

    }
  }
}

}
