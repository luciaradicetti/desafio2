import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MoviesService } from 'src/app/services/movies/movie.service';
import { Result, TvPopular } from '../../interfaces/TvPopular';

@Component({
  selector: 'app-vista',
  templateUrl: './vista.component.html',
  styleUrls: ['./vista.component.css'],
  providers : [MoviesService]
})
export class VistaComponent implements OnInit {
  id!:any;
  tv!: Result[];
  titulo:string="";
  imagen!:any;
  resena:string="";
  calificacion: number=0;
  fecha!:Date;

  constructor(private route : ActivatedRoute, private _movieService:MoviesService) { }

  ngOnInit(): void {
    this.route.url.subscribe(params =>{
      this.id = params[1].path;
    });
    this.getSeries();
  }

  getSeries(){
    this._movieService.getSeries().subscribe({
      next: (data : TvPopular) =>{
        this.tv=data.results;
        this.datos();
      },
      error: (err)=>{
        console.log(err);
      }
    })

  }

  datos(){
    for(let card of this.tv){
      if(this.id == card.id){
        this.titulo = card.name;
        this.imagen = card.poster_path;
        this.resena = card.overview;
        this.calificacion = card.vote_average;
        this.fecha = card.first_air_date;
      }
    }
  }
}
