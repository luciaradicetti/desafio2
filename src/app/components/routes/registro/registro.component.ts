import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/Auth/auth.service';
import { MoviesService } from 'src/app/services/movies/movie.service';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css'],
  providers : [AuthService]
})
export class RegistroComponent implements OnInit {
  miFormulario: FormGroup = this.formulario.group({
    email: [, [Validators.required, Validators.minLength(6)]],
    password: [, [Validators.required, Validators.minLength(6)]],
  });
  bandera:boolean=false;

  constructor( private formulario: FormBuilder, private _authService:AuthService, private firestore:MoviesService, private router : Router) { }

  ngOnInit(): void {
  }
  campoEsValido(campo: string) {
    return (
      this.miFormulario.controls[campo].errors &&
      this.miFormulario.controls[campo].touched
    );
  }
  ingreso(){
    this.router.navigate([`ingreso`]);
  }

  guardar() {
    if (this.miFormulario.invalid) {
      this.miFormulario.markAllAsTouched();
      return;
    }
    this._authService.register(this.miFormulario.value.email,this.miFormulario.value.password).then(res =>{
      if(!res)
        return
    console.log("Se registro: ", res);
    localStorage.setItem('Usuario',JSON.stringify(res.user));
    this.miFormulario.reset();
    this.router.navigate([`inicio`]);
  })
  }


}
