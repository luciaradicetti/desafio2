import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MoviesService } from 'src/app/services/movies/movie.service';
import { Result } from '../../interfaces/MoviePopular';
import { MoviePopular } from '../../interfaces/MoviePopular';

@Component({
  selector: 'app-vistapelis',
  templateUrl: './vistapelis.component.html',
  styleUrls: ['./vistapelis.component.css']
})
export class VistapelisComponent implements OnInit {
  id!:any;
  movies!: Result[];
  titulo:string="";
  imagen!:any;
  resena:string="";
  calificacion: number=0;
  fecha!:Date;

  constructor(private route : ActivatedRoute, private _movieService:MoviesService) { }

  ngOnInit(): void {
    this.route.url.subscribe(params =>{
      this.id = params[1].path;
    });
    this.getMovie();
  }

  getMovie(){
    this._movieService.getMovie().subscribe({
      next: (data : MoviePopular) =>{
        this.movies=data.results;
        this.datos();
      },
      error:(err) =>{
        console.log(err)
      }
    })
  }

  datos(){
    for(let card of this.movies){
      if(this.id == card.id){
        this.titulo = card.original_title;
        this.imagen = card.poster_path;
        this.resena = card.overview;
        this.calificacion = card.vote_average;
        this.fecha = card.release_date;
      }
    }
  }
}
