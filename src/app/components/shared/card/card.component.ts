import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { MoviesService } from 'src/app/services/movies/movie.service';
import { MovieSerieBase} from '../../interfaces/MovieSerie';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {
 @Input() name?:string='';
 @Input() image:string='';
 @Input() rating?:number=0;
 @Input() id?:string="0";
 @Input() categoria: string ='';
 @Input() lista?:boolean;
 @Input() agregado?:boolean;
 @Input() modal:boolean=false;
 user!:any;
 card!:MovieSerieBase;
 arreglo!:MovieSerieBase[];
 idGlobal!:any;
 arreMovie!: MovieSerieBase[];


  constructor(private router : Router, private tv_movie: MoviesService) { }

  ngOnInit(): void {
    
  }

  public goTo(id?:number|string, categoria?:string){
    if(categoria == 'tv'){
      this.router.navigate([`vista/${id}`]);
    }else{
      this.router.navigate([`vistapelis/${id}`]);
    }
  }
 
        public agrego(){
          this.lista=false;
          this.agregado=true;
          this.user = JSON.parse(localStorage.getItem('Usuario')  || '');
          console.log(this.user.uid)
          this.card={id: this.id, name: this.name,
          poster_path: this.image,
          title: this.name,
          vote_average: this.rating,
          type: this.categoria}
          this.tv_movie.addMovieSerie(this.user.uid,this.card,this.categoria).then(()=>{
            console.log("Se agrego item")
          }).catch(error =>{
            console.log("error",error);
          })
        }
  eliminar(){
    this.tv_movie.deleteItem(this.user.uid,this.idGlobal,this.categoria).then( () =>{
      console.log("Se elimino")
    }).catch(error =>{
      console.log("Error",error)
    });
  }

  public elimino(){
    this.user = JSON.parse(localStorage.getItem('Usuario')  || '');
    this.idGlobal = this.tv_movie.getList(this.user.uid,this.categoria).subscribe(response => {
      response.forEach((element:any) => {
        if((element.payload.doc.data().id) == this.id){
          this.idGlobal=element.payload.doc.id;
          console.log(this.idGlobal);
          this.agregado=false;
          this.lista=true;
          this.eliminar();
        }
      });
    });
  }

}
