export interface MovieSerie extends MovieSerieBase {
    backdrop_path: string;
    original_language: string;
    original_title: string;
    overview: string;
    release_date: string;
    media_type: string;
}

export interface MovieSerieBase{
    id?: string;
    name?: string;
    poster_path: string;
    title?: string;
    vote_average?: number;
    type?:string;
}

export interface MovieSerieUser extends MovieSerieBase{
    idUser?: string;
}