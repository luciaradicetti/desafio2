import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/services/Auth/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  public userLogged: Observable<any> = this.authService.afauth.user;

  constructor(private authService: AuthService,private router : Router) { }

  ngOnInit(): void {

  }
   public logOut(){
     this.authService.logout();
     this.router.navigate([`inicio`]);
   }
   public agregar(){
    this.router.navigate([`agregar`]);
   }
}
