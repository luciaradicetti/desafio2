import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardComponent } from './card/card.component';
import { VistaComponent } from './vista/vista.component';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { RouterModule } from '@angular/router';
import { VistapelisComponent } from './vistapelis/vistapelis.component';



@NgModule({
  declarations: [
    CardComponent,
    VistaComponent,
    VistapelisComponent,


  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    RouterModule,

  ],
  exports:[
    CardComponent,
    VistaComponent

  ]
})
export class SharedModule { }
