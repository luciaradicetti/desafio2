import { Component, OnInit } from '@angular/core';
import { MoviesService } from 'src/app/services/movies/movie.service';
import { MovieSerieBase } from '../../interfaces/MovieSerie';

@Component({
  selector: 'app-listado-series',
  templateUrl: './listado-series.component.html',
  styleUrls: ['./listado-series.component.css']
})
export class ListadoSeriesComponent implements OnInit {
  movies!: MovieSerieBase[] ;
  elemento!:string;
  filtro:MovieSerieBase[]=[];
  filtro_busqueda:MovieSerieBase[]=this.movies;
  tv:string="tv";
  user!:any;

  constructor(private _movieService:MoviesService) { }

  ngOnInit(): void {
    this.getMovie()
  }

  getMovie(){
    this.user = JSON.parse(localStorage.getItem('Usuario')  || '');
    this._movieService.getList(this.user.uid,"tv").subscribe(
      response => {
        this.movies =[];
        console.log("Esto es response",response)
        response.forEach((elemento:any)=>{
          console.log(elemento.payload.doc.data())
            this.movies.push({
              idglobal: elemento.payload.doc.id,
              ...elemento.payload.doc.data()
            })
        })
        console.log("Esta es la peticion de movies", this.movies)
        this.filtro_busqueda=this.movies;
      },
      error =>{
        console.log("Error peticion", error)
      }
    )
  }
  public contador(){
    return this.movies.length
  }
  
  public busqueda(valor:string){
    this.elemento = valor.toUpperCase();
    this.filtro=[];
    for(let card of this.movies){
      if(card.title?.toUpperCase().includes(this.elemento)){
          this.filtro.push(card);
       
      }
      if(valor != ''){
        this.filtro_busqueda=this.filtro;
        
      }else{
        this.filtro_busqueda=this.movies;
    
      }
    }
  }
}
